/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {SVGManager} from '@ohos/XmlGraphicsBatikETS';
import {SVGRect} from '@ohos/XmlGraphicsBatikETS';
import {SVGSpecifiedFormat} from '@ohos/XmlGraphicsBatikETS';
import {consoleInfo} from '@ohos/XmlGraphicsBatikETS';
import {SVGAttrConstants} from '@ohos/XmlGraphicsBatikETS';

@Entry
@Component
struct Index {
  private svgManager: SVGManager = SVGManager.getInstance();
  private allAttrRectObj: object = Object.create(null);
  @State
  private svgUri: string= '';

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Image(this.svgUri)
        .backgroundColor(Color.Pink)
        .width(200)
        .height(200)

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('add rect1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var rect: SVGRect = new SVGRect();
            rect.setX(10);
            rect.setY(10);
            rect.setRX(20);
            rect.setRY(20);
            rect.setWidth(100);
            rect.setHeight(100);
            rect.addAttribute('style', 'fill:rgb(0,0,255);stroke-width:2;stroke:rgb(0,0,0)')
            var rectObj = rect.toObj();

            var svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('rect');
            svgSpecifiedFormat.setAttributes(rectObj);

            var svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.allAttrRectObj = svgSpecifiedFormat.toObj();
              this.svgManager.addChildNode(svgRoot, this.allAttrRectObj);
              consoleInfo('Test rect: add rect1 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })

        Button('add rect2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var rect: SVGRect = new SVGRect();
            rect.setX(150);
            rect.setY(10);
            rect.setRX(40);
            rect.setRY(40);
            rect.setWidth(50);
            rect.setHeight(50);
            rect.addAttribute('style', 'fill:rgb(255,0,0);stroke-width:5;stroke:rgb(0,0,255)')
            var rectObj = rect.toObj();

            var svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('rect');
            svgSpecifiedFormat.setAttributes(rectObj);

            var svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.svgManager.addChildNode(svgRoot, svgSpecifiedFormat.toObj());
              consoleInfo('Test rect: add rect2 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })
      }
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('update attr for rect1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test rect: update attr for rect1', 'svg tag is null');
              return false;
            }

            var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);
            if (!svgElements) {
              consoleInfo('Test rect: update attr for rect1', `svg tag's elements is null`);
              return false;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !Array.isArray(svgElements)) {
              consoleInfo('Test rect: update attr for rect1', `the elements's type of svg tag is not array`);
              return;
            }

            var rectResult = null;
            try {
              svgElements.forEach((item) => {
                if (typeof item === SVGAttrConstants.TYPEOF_OBJECT) {
                  var nameValue: string = this.svgManager.getValueForKey(item, SVGAttrConstants.ATTR_KEY_NAME);
                  if (nameValue === 'rect') {
                    rectResult = item;
                    throw 'has got rect,jump out';
                  }
                }
              })
            } catch (e) {
              if (!rectResult) {
                consoleInfo('Test rect: update attr for rect1', 'rect not exist');
                return;
              }

              if (typeof rectResult === SVGAttrConstants.TYPEOF_OBJECT) {
                var rectAttributes = rectResult[SVGAttrConstants.ATTR_KEY_ATTRIBUTES];
                rectAttributes['x'] = 20;
                rectAttributes['y'] = 20;
                rectAttributes['rx'] = 10;
                rectAttributes['ry'] = 50;
                rectAttributes['width'] = 80;
                rectAttributes['height'] = 80;
                this.svgManager.setAttribute(rectAttributes, 'style', 'fill:rgb(0,255,0);stroke-width:10;stroke:rgb(0,255,255)');
                this.allAttrRectObj = rectResult;
              }
              consoleInfo('Test rect: update attr for rect1 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })

        Button('remove rect2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test rect: remove rect2', 'svg tag is null');
              return;
            }

            var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);
            if (!svgElements) {
              consoleInfo('Test rect: remove rect2', `svg tag's elements is null`);
              return;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !Array.isArray(svgElements)) {
              consoleInfo('Test rect: remove rect2', `svg tag's elements is null`);
              return;
            }

            if (svgElements.length >= 2) {
              svgElements.splice(1, 1);
            }
            consoleInfo('Test remove rect2 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Button('show svg with rect')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .width(250)
        .height(50)
        .onClick(() => {
          var svgTotalObj = this.svgManager.getSVGTotalObj();
          var success = function () {
            consoleInfo('Test rect: saveFile', 'success');
          }
          this.svgManager.saveSVG('svgWithRect.svg', svgTotalObj, success);
          this.svgUri = '';
          setTimeout(() => {
            this.svgManager.getFilePath((filePath) => {
              consoleInfo('Test rect: show svg getFilePath ', filePath);
              this.svgUri = 'file://' + filePath + '/svgWithRect.svg';
            })
          }, 100);
        })


      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove x attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('x');
          })

        Button('remove y attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('y');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove rx attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('rx');
          })

        Button('remove ry attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('ry');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Button('remove rx ry attr')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .width(250)
        .height(50)
        .onClick(() => {
          this.removeAttribute('rx', 'ry');
        })

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove width attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('width');
          })

        Button('remove height attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('height');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    // 清空已存在的SVG根
    this.svgManager.createSVGDeclares();
  }

  removeAttribute(firstAttrName: string, secondAttrName?: string) {
    if (!this.allAttrRectObj) {
      consoleInfo('test remove ' + firstAttrName, 'Rect is not added.');
      return;
    }
    var rectJson = JSON.stringify(this.allAttrRectObj);
    var rectOriginData = JSON.parse(rectJson);

    var attrs = this.svgManager.getValueForKey(rectOriginData, SVGAttrConstants.ATTR_KEY_ATTRIBUTES);
    if (!attrs) {
      consoleInfo('test remove ' + firstAttrName, 'rect1 has no attributes');
      return;
    }
    this.svgManager.removeByKey(attrs, firstAttrName);
    if (secondAttrName) {
      this.svgManager.removeByKey(attrs, secondAttrName);
    }

    // 替换 rect
    var svgRoot = this.svgManager.getSVGRoot();
    var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);

    if (typeof svgElements === SVGAttrConstants.TYPEOF_OBJECT && Array.isArray(svgElements)) {
      svgElements.splice(0, 1, rectOriginData);
    }
    consoleInfo('test remove attr: ' + firstAttrName, JSON.stringify(this.svgManager.getSVGTotalObj()));
  }
}